﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using GetOrderForAllSources;

namespace GetOrders
{
    public partial class ServiceGetOrder : ServiceBase
    {
        private System.Timers.Timer Timer;
        internal const String LogName = "SIAGetOrdersServiceLog1";
        internal const string SourceName = "SIAGetOrdersService1";
        public ServiceGetOrder()
        {
           
            try
            {
                //if (!EventLog.SourceExists(SourceName))
                //{
                //    EventSourceCreationData sourceData = new EventSourceCreationData(SourceName, "SIAGetOrdersServiceLog1");
                //    EventLog.CreateEventSource(sourceData);
                //}
                //logger = new EventLog(LogName, ".", SourceName);
                //log = new System.Diagnostics.EventLog("Application");
                //log.WriteEntry("Start");
                InitializeComponent();
                try
                {
                    //log.WriteEntry("ReadAppConfig");
                    ReadAppConfig appcfg = new ReadAppConfig();
                    //log.WriteEntry("StartTimer");
                    Timer = new System.Timers.Timer();
                    Timer.Interval = Convert.ToInt16(appcfg.TimeOut) * 60 * 1000;
                    Timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerElapsed);
                    Timer.Start();

                }
                catch(Exception e)
                {
                    System.IO.File.AppendAllText("c:\\temp\\logs.log", e.Message);
                }

            }
            catch (Exception e)
            {
                System.IO.File.AppendAllText("c:\\temp\\logs.log", e.Message);
            }
           
        }
        void OnTimerElapsed(Object sender,System.Timers.ElapsedEventArgs e)
        {
            try
            {
             ReadAppConfig appcfg = new ReadAppConfig();
                (sender as System.Timers.Timer).Stop();
            TimeSpan nowtime = DateTime.Now.TimeOfDay;
            TimeSpan starttime = new TimeSpan(Convert.ToInt16(appcfg.StartTime.Split(':')[0]),Convert.ToInt16(appcfg.StartTime.Split(':')[1]),0);
            TimeSpan endtime = new TimeSpan(Convert.ToInt16(appcfg.EndTime.Split(':')[0]), Convert.ToInt16(appcfg.EndTime.Split(':')[1]), 0);
            if (starttime <= nowtime && nowtime <= endtime)
            {
                GetOrderForAllSources.Processing processing = new Processing();
                processing.DoProcessing();
            }
            (sender as System.Timers.Timer).Start();
            }
            catch(Exception ex)
            {
                System.IO.File.AppendAllText("c:\\temp\\logs.log", ex.Message);
            }
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }
    }
}
